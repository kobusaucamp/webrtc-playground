// const WebSocket = require('ws');

// let users = {};

// const wss = new WebSocket.Server({ port: 8080 });

// wss.on('connection', function connection(ws) {

//     console.log('User connected');

//     ws.on('message', function incoming(message) {
//         var data;

//         console.log('received: %s', message);

//         try {
//             data = JSON.parse(message);
//         } catch (e) {
//             console.log('Error parsing JSON!!!');
//             data = {};
//         }
//         console.log(data);

//         wss.clients.forEach(function each(client) {

//             if (client.readyState == WebSocket.OPEN) {

//                 switch (data.type) {
//                     case 'login':
//                         console.log(`User logged in as ${data.name}`);
//                         if (users[data.name]) {
//                             sendTo(ws, {
//                                 type: 'login',
//                                 success: false
//                             });
//                         } else {
//                             users[data.name] = client;
//                             client.name = data.name;
//                             sendTo(ws, {
//                                 type: 'login',
//                                 success: true
//                             });
//                         }
//                         break;
//                     case 'offer':
//                         console.log(`Sending offer to ${data.name}`);
//                         if (users[data.name] != null) {
//                             client.otherName = data.name;
//                             sendTo(ws, {
//                                 type: 'offer',
//                                 offer: data.offer,
//                                 name: connection.name
//                             });
//                         }
//                         break;
//                     case 'answer':
//                         console.log(`Sending answer to ${data.name}`);
//                         if (users[data.name] != null) {
//                             connection.otherName = data.name;
//                             sendTo(ws, {
//                                 type: 'answer',
//                                 answer: data.answer
//                             });
//                         }
//                         break;
//                     default:
//                         sendTo(ws, {
//                             type: 'error',
//                             message: `Unrecognized command: ${data.type}`
//                         });
//                         break;
//                 }

//             }
//         });


//     });

// });

// function sendTo(client, message) {
//     client.send(JSON.stringify(message));
// }

// wss.on('close', function close() {
//     console.log('disconnected');
// });

var video = document.querySelector("#videoElement");

if (navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia({
            video: true
        })
        .then(function (stream) {
            video.srcObject = stream;
        })
        .catch(function (err0r) {
            console.log("Something went wrong!");
        });
}